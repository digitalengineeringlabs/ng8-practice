import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicles',
    template: `
        <!-- add below styles to DIV using ngStyle -->
        <!-- {'padding':'10px', 'margin-top':'10px', 'font-family':'Verdana'} -->
        <!-- apply the classes, available or notAvailable based on rented flat -->
        <!-- DIV is for each vehicle, hence use buitin directive to render this for each item in array-->
        <div>
            <h3><Model></h3>
            <p><Color> <Type> with <Seats> seats</p>
            <p>$<Cost>/Km</p>
            <div>
            <button>Cancel Booking</button> <!-- for rented vehicle -->
            <button>Book Now</button> <!-- for non rented vehicle -->
            </div>
        </div>
    `,
    styles: [`
        .available {
            color: white;
            background-color: green;
        }
        .notAvailable {
            color: yellow;
            background-color: #c66;
        }
    `]
})
export class VehiclesComponent {
    vehicles = [
        {
            id: '1',
            model: 'BMW X3',
            type: 'SUV',
            rented: true,
            color: 'red',
            seats: 7,
            costPerKm: 5 
        },
        {
            id: '2',
            model: 'Audi A4',
            type: 'Car',
            rented: false,
            color: 'white',
            seats: 5,
            costPerKm: 6.5
        },
        {
            id: '3',
            model: 'Mini cooper',
            type: 'Car',
            rented: false,
            color: 'blue',
            seats: 2,
            costPerKm: 4
        }
    ]

    // Event Handler methods
}