# String Interpolation

Copy the [app-vehicle-info](vehicle-info.component.ts) component file into your app folder
Do the necessary configuration

## Activity 1

Use String Interpolation to render the fields from component

1. model
2. type & seats
3. color

## Activity 2

rented flag should be rendered as "Available" or "Not Available", for value true or false respectively

# Property Data Binding

Copy the [app-vehicle-edit](vehicle-edit.component.ts) component file into your app folder
Do the necessary configuration

## Activity 1

Use property binding to map the values from component to the input fields

## Activity 2

Use String Interpolation to render the values from component, into the section below in HTML

# Event Data Binding

Use same app-vechile-edit component 

## Activity 1

Add event bindings to input fields in app-vehicle-edit component

## Activity 2

On clicking submit button, print the values from properties into console

# Two-way Data Binding

Copy the [app-vehicle-add](vehicle-add.component.ts) component file into your app folder
Do the necessary configuration

## Activity 1

Add two-way data binding using ngModel from FormsModule

## Activity 2

Render the values from properties on view, using String interpolation