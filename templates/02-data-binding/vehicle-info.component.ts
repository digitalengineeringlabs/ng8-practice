import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicle-info', 
    template: `
        <div>
            <!-- model -->
            <h3>Model</h3>
            <!-- type and seats -->
            <p>Type, Seats</p>
            <!-- show "Available" if rented is true, otherwise "Not Available" -->
            <p>Text</p>
            <!-- color -->
            <p>Color</p>
        </div>
    `
}) 
export class VehicleInfoComponent {   
    info = {
            id: 'B001',
            model: 'BMW X3',
            type: 'SUV',
            rented: true,
            color: 'red',
            seats: 5
        }
}