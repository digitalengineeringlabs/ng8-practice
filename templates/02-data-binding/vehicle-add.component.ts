import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicle-add',
    template: `
        <div>
            <!-- Use Two-way Data Binding -->
            <p>Model: <select>
                    <option>Select</option>
                    <option>BMW X3</option>
                    <option>Audi A2</option>
            </select></p>
            <p>Type: <input type='radio' name='type' value='SUV'>SUV
                    <input type='radio' name='type' value='Car'>Car</p>
            <p>Color: <input type='color'></p>
            <p>Seats: <input type='text'></p>
            <!-- On clicking this button, show the latest values from input fields onto console-->
            <button>Submit</button>
        </div>
        <br/>
        <!-- Fill values using String interpolation -->
        <div>
            Model: <br/>
            Type: <br/>
            Color: <br/>
            Seats: 
        </div>
    `
})
export class VehicleAddComponent {
    info = {
        id: '',
        model: '',
        type: '',
        rented: false,
        color: '',
        seats: 0
    }
}