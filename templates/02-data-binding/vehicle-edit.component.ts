import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicle-edit',
    template: `
        <div>
        <!-- Use Property Binding and Event Binding -->
            <p>Model: <select>
                <option>Select</option>
                <option>BMW X3</option>
                <option>Audi A2</option>
                </select></p>
            <p>Type: <input type='radio' name='type' value='SUV'>SUV
                    <input type='radio' name='type' value='Car'>Car</p>
            <p>Color: <input type='color'></p>
            <p>Seats: <input type='text'></p>
            <!-- On clicking this button, show the latest values from input fields onto console-->
            <button>Submit</button>
            </div>
        <br/>
        <!-- Fill values using String interpolation -->
        <div>
            Model: <br/>
            Type: <br/>
            Color: <br/>
            Seats: 
        </div>
    `
})
export class VehicleEditComponent {
    info = {
        id: 'B001',
        model: 'Audi A2',
        type: 'Car',
        rented: true,
        color: '#226699',
        seats: 5
    }

    isType(type) {
        return type == this.info.type ? true : false;
    }

    isModel(model) {
        return model == this.info.model ? true : false;
    }

}