# Activity 1

Create a component "app-user-menu" without using CLI command
Component selector of type "attribute"

The component must render view with below HTML

        <ul>
            <li>My Profile</li>
            <li>Log Out</li>
        </ul>

Include this component in App component template

# Activity 2

Create another component "app-user-auth" without using CLI command
Component selector of type "element"

It must render below HTML, use "app-user-menu" inside

        <div>
            <span>Welcome Mike</span>
            <div app-user-menu></div>
        </div>

# Activity 3

In app-user-auth component, add below style

        .user-auth {
            background-color: #ddd;
        }

Apply the style to app-user-auth component

        <div class="user-auth">
            <span>Welcome Mike</span>
            <app-user-menu></app-user-menu>
        </div>

# Activity 4

Create another component "app-user-menu-item" using CLI command
Component selector of type "class"

Below is the view template

        <div>Preferences</div>

Update app-user-menu component to include this component as menu item

        <ul>
            <li>My Profile</li>
            <li class="app-user-menu-item"></li>
            <li>Log Out</li>
        </ul>

# Conditions

Use only template and styles property in @Component decorator in manual component creation