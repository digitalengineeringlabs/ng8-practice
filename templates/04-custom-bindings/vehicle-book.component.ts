import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-vehicle-book',
    template: `
    <div>
        <h3>{{vehicle.model}}</h3>
        <p>{{vehicle.color}} {{vehicle.type}} with {{vehicle.seats}} seats</p>
        <p>{{'$'}}{{vehicle.costPerKm}}/Km</p>
        <div>
        <!-- Based on rented flag-->
        <!-- <button>Cancel Booking</button> OR <button>Book Now</button>-->
        </div>
    </div>
    `,
    styles: [`
        .available {
            color: white;
            background-color: green;
        }
        .notAvailable {
            color: yellow;
            background-color: #c66;
        }
    `]
})
export class VehicleBookComponent{

    // define input property
    // define output event

    // event handlers to handle book and cancel actions

}