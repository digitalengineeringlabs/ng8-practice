import { Component } from '@angular/core';

@Component({
    selector: 'app-vehicle-listing',
    template: `
        <!-- should use ngFor to render component for every item in array -->
        <!-- should pass the vehicle object to component via property binding -->
        <!-- should handle event to manulipulate array to change the rented to true or false -->
        <app-vehicle-book></app-vehicle-book>
    `
})
export class VehicleListingComponent {
    vehicles = [
        {
            id: '1',
            model: 'BMW X3',
            type: 'SUV',
            rented: true,
            color: 'red',
            seats: 7,
            costPerKm: 5 
        },
        {
            id: '2',
            model: 'Audi A4',
            type: 'Car',
            rented: false,
            color: 'white',
            seats: 5,
            costPerKm: 6.5
        },
        {
            id: '3',
            model: 'Mini cooper',
            type: 'Car',
            rented: false,
            color: 'blue',
            seats: 2,
            costPerKm: 4
        }
    ]

    handleBooking(data){
        console.log('Data ',data);
        // the data will be of format {id:'',rented:boolean}
        // update the respective item in array
    }

}